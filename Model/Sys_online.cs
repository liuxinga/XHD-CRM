/*
* Sys_online.cs
*
* 功 能： N/A
* 类 名： Sys_online
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-23 18:38:21    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;

namespace XHD.Model
{
    /// <summary>
    ///     Sys_online:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public class Sys_online
    {
        #region Model

        private DateTime? _lastlogtime;
        private int? _userid;
        private string _username;

        /// <summary>
        /// </summary>
        public int? UserID
        {
            set { _userid = value; }
            get { return _userid; }
        }

        /// <summary>
        /// </summary>
        public string UserName
        {
            set { _username = value; }
            get { return _username; }
        }

        /// <summary>
        /// </summary>
        public DateTime? LastLogTime
        {
            set { _lastlogtime = value; }
            get { return _lastlogtime; }
        }

        #endregion Model
    }
}