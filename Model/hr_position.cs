﻿/*
* hr_position.cs
*
* 功 能： N/A
* 类 名： hr_position
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-24 10:22:47    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;

namespace XHD.Model
{
    /// <summary>
    ///     hr_position:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public class hr_position
    {
        #region Model

        private DateTime? _create_date;
        private int? _create_id;
        private DateTime? _delete_time;
        private int _id;
        private int? _isdelete;
        private string _position_level;
        private string _position_name;
        private int? _position_order;

        /// <summary>
        /// </summary>
        public int id
        {
            set { _id = value; }
            get { return _id; }
        }

        /// <summary>
        /// </summary>
        public string position_name
        {
            set { _position_name = value; }
            get { return _position_name; }
        }

        /// <summary>
        /// </summary>
        public int? position_order
        {
            set { _position_order = value; }
            get { return _position_order; }
        }

        /// <summary>
        /// </summary>
        public string position_level
        {
            set { _position_level = value; }
            get { return _position_level; }
        }

        /// <summary>
        /// </summary>
        public int? create_id
        {
            set { _create_id = value; }
            get { return _create_id; }
        }

        /// <summary>
        /// </summary>
        public DateTime? create_date
        {
            set { _create_date = value; }
            get { return _create_date; }
        }

        /// <summary>
        /// </summary>
        public int? isDelete
        {
            set { _isdelete = value; }
            get { return _isdelete; }
        }

        /// <summary>
        /// </summary>
        public DateTime? Delete_time
        {
            set { _delete_time = value; }
            get { return _delete_time; }
        }

        #endregion Model
    }
}