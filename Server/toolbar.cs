﻿/*
* toolbar.cs
*
* 功 能： N/A
* 类 名： toolbar
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-23 18:38:21    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System.Data;
using System.Web;
using XHD.Common;
using XHD.Controller;

namespace XHD.Server
{
    public class toolbar
    {
        public static Model.CRM_Contact model = new Model.CRM_Contact();
        private readonly BLL.Sys_Button btn = new BLL.Sys_Button();

        public HttpContext Context;
        public int emp_id;
        public string emp_name;
        public Model.hr_employee employee;
        public HttpRequest request;
        public string uid;


        public toolbar()
        {
        }

        public toolbar(HttpContext context)
        {
            Context = context;
            request = context.Request;

            var userinfo = new User_info();
            employee = userinfo.GetCurrentEmpInfo(context);

            emp_id = employee.ID;
            emp_name = PageValidate.InputText(employee.name, 50);
            uid = PageValidate.InputText(employee.uid, 50);
        }

        public string GetSys()
        {
            int mid = int.Parse(request["mid"]);
            bool BtnAble = false;

            if (uid == "admin")
            {
                BtnAble = true;
            }

            DataSet ds = btn.GetList(0, "Menu_id = " + int.Parse(request["mid"]), "Btn_order");
            var getauth = new GetAuthorityByUid();
            string toolbarscript = "{Items:[";
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                toolbarscript += "{";
                toolbarscript += "type: 'button',";
                toolbarscript += "text: '" + ds.Tables[0].Rows[i]["Btn_name"] + "',";
                toolbarscript += "icon: '" + ds.Tables[0].Rows[i]["Btn_icon"] + "',";
                if (BtnAble)
                {
                    toolbarscript += "disable: true,";
                }
                else
                {
                    toolbarscript += "disable: " +
                                     getauth.GetBtnAuthority(emp_id.ToString(),
                                         ds.Tables[0].Rows[i]["Btn_id"].ToString()).ToString().ToLower() + ",";
                }
                toolbarscript += "click: function() {";
                toolbarscript += ds.Tables[0].Rows[i]["Btn_handler"].ToString();
                toolbarscript += "}";
                toolbarscript += "},";
            }
            toolbarscript = toolbarscript.Substring(0, toolbarscript.Length - 1);
            toolbarscript += "]}";

            return (toolbarscript);
        }
    }
}