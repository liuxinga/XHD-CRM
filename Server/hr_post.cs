﻿/*
* hr_post.cs
*
* 功 能： N/A
* 类 名： hr_post
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-23 18:38:21    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System.Data;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using XHD.Common;
using XHD.Controller;

namespace XHD.Server
{
    public class hr_post
    {
        public static BLL.hr_post post = new BLL.hr_post();
        public static Model.hr_post model = new Model.hr_post();
        private readonly BLL.hr_employee emp = new BLL.hr_employee();

        public HttpContext Context;
        public int emp_id;
        public string emp_name;
        public Model.hr_employee employee;
        public HttpRequest request;
        public string uid;


        public hr_post()
        {
        }

        public hr_post(HttpContext context)
        {
            Context = context;
            request = context.Request;

            var userinfo = new User_info();
            employee = userinfo.GetCurrentEmpInfo(context);

            emp_id = employee.ID;
            emp_name = PageValidate.InputText(employee.name, 50);
            uid = PageValidate.InputText(employee.uid, 50);
        }

        public string grid()
        {
            string depid = request["depid"];
            string emps = request["empstatus"];
            int empstuats = 0;
            if (PageValidate.IsNumber(emps))
                empstuats = int.Parse(emps);

            string serchtxt = "";

            switch (empstuats)
            {
                case 0:
                    serchtxt += "1=1 ";
                    break;
                case 1:
                    serchtxt += "emp_id=-1 ";
                    break;
                case 2:
                    serchtxt += "emp_id!=-1 ";
                    break;
            }

            if (!string.IsNullOrEmpty(depid) && depid != "null")
                serchtxt += " and dep_id=" + int.Parse(depid);

            DataSet ds = post.GetList(0, serchtxt, " position_order");
            string dt = GetGridJSON.DataTableToJSON(ds.Tables[0]);
            return dt;
        }


        public void save()
        {
            //dep
            model.dep_id = int.Parse(request["T_depname_val"]);
            //name
            model.post_name = PageValidate.InputText(request["T_postname"], 250);
            //position
            model.position_id = int.Parse(request["T_position_val"]);
            //emp
            int empid = PageValidate.IsNumber(request["T_emp_val"]) ? int.Parse(request["T_emp_val"]) : -1;
            model.emp_id = empid;
            //note
            model.note = PageValidate.InputText(request["T_descript"], 4000);

            var modelemp = new Model.hr_employee();
            //更新员工岗位
            modelemp.d_id = model.dep_id;

            modelemp.position_id = model.position_id;
            modelemp.ID = empid;

            string postid = PageValidate.InputText(request["postid"], 50);
            if (PageValidate.IsNumber(postid))
            {
                model.id = int.Parse(postid);
                DataSet ds = post.GetList(" id=" + int.Parse(postid));
                DataRow dr = null;
                if (ds.Tables[0].Rows.Count > 0)
                {
                    dr = ds.Tables[0].Rows[0];

                    //判断默认岗位
                    if (model.emp_id == -1)
                    {
                        model.default_post = 0;
                    }
                    else
                    {
                        DataSet ds1 =
                            post.GetList(string.Format("default_post=1 and emp_id={0} and id!={1}", model.emp_id,
                                int.Parse(postid)));
                        if (ds1.Tables[0].Rows.Count > 0)
                            model.default_post = 0; //此员工有默认岗位  
                        else
                        {
                            model.default_post = 1; //设置此岗位为此员工默认岗位 

                            //更新员工岗位
                            modelemp.postid = model.id;
                            emp.UpdatePost(modelemp);
                        }
                    }
                }
                post.Update(model);

                //日志
                var log = new sys_log();

                int UserID = emp_id;
                string UserName = emp_name;
                string IPStreet = request.UserHostAddress;
                string EventTitle = model.post_name;
                string EventType = "岗位修改";
                int EventID = model.id;
                string Log_Content = null;

                if (dr["post_name"].ToString() != request["T_postname"])
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "岗位名称", dr["post_name"], request["T_postname"]);

                if (dr["position_name"].ToString() != request["T_position"])
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "岗位级别", dr["position_name"], request["T_position"]);

                if (dr["emp_name"].ToString() != request["T_emp"])
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "岗位员工", dr["emp_name"], request["T_emp"]);

                if (dr["note"].ToString() != request["T_descript"])
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "描述", dr["note"], request["T_descript"]);

                if (!string.IsNullOrEmpty(Log_Content))
                    log.Add_log(UserID, UserName, IPStreet, EventTitle, EventType, EventID, Log_Content);
            }
            else
            {
                model.isDelete = 0;
                postid = post.Add(model).ToString();

                //判断默认岗位
                if (model.emp_id == -1)
                {
                    model.default_post = 0;
                }
                else
                {
                    DataSet ds1 =
                        post.GetList(string.Format("default_post=1 and emp_id={0} and id!={1}", model.emp_id,
                            int.Parse(postid)));
                    if (ds1.Tables[0].Rows.Count > 0)
                        model.default_post = 0; //此员工有默认岗位  
                    else
                    {
                        model.default_post = 1; //设置此岗位为此员工默认岗位 

                        //更新员工岗位
                        modelemp.postid = int.Parse(postid);
                        emp.UpdatePost(modelemp);
                    }
                }
                post.UpdatePostEmp(model);
            }
        }

        //Form JSON
        public string form(string id)
        {
            string dt;

            if (PageValidate.IsNumber(id))
            {
                DataSet ds = post.GetList("id=" + id);

                dt = DataToJson.DataToJSON(ds);
            }
            else
            {
                dt = "{}";
            }

            return dt;
        }

        //del
        public string del(int id)
        {
            string EventType = "岗位删除";
            DataSet ds = post.GetList(" id=" + id);

            if (ds.Tables[0].Rows[0]["emp_id"].ToString() != "-1")
            {
                //含有员工信息不能删除
                return ("false:emp");
            }
            bool isdel = post.Delete(id);
            if (isdel)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    int UserID = emp_id;
                    string UserName = emp_name;
                    string IPStreet = request.UserHostAddress;
                    int EventID = id;
                    string EventTitle = ds.Tables[0].Rows[i]["post_name"].ToString();

                    var log = new sys_log();
                    log.Add_log(UserID, UserName, IPStreet, EventTitle, EventType, EventID, null);
                }
                return ("true");
            }
            return ("false");
        }

        //get post by empid
        public string getpostbyempid(int id)
        {
            var hp = new BLL.hr_post();
            DataSet ds = hp.GetList(" emp_id=" + id);
            if (ds.Tables[0].Rows.Count > 0)
            {
                string dt = GetGridJSON.DataTableToJSON(ds.Tables[0]);
                return dt;
            }
            return null;
        }

        //serch
        public string serch(string serchtxt)
        {
            var hp = new BLL.hr_post();
            serchtxt = PageValidate.InputText(serchtxt, 255);
            DataSet ds = hp.GetList(" isDelete=0 and post_name like N'%" + serchtxt + "%'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                string dt = GetGridJSON.DataTableToJSON(ds.Tables[0]);
                return dt;
            }
            return ("{Rows:[],Total:0}");
        }

        //post_emp
        public void postemp()
        {
            string json = request["PostData"].ToLower();
            var js = new JavaScriptSerializer();

            PostData[] postdata;
            postdata = js.Deserialize<PostData[]>(json);

            var hp = new BLL.hr_post();


            string empid = request["empid"];
            int Eid = -1;
            if (!string.IsNullOrEmpty(empid))
            {
                Eid = PageValidate.IsNumber(empid) ? int.Parse(empid) : -1;
            }
            var modelemp = new Model.hr_employee();
            model.emp_id = Eid;
            modelemp.ID = Eid;

            for (int i = 0; i < postdata.Length; i++)
            {
                model.id = postdata[i].Post_id;
                model.default_post = postdata[i].Default_post;

                if (postdata[i].Default_post == 1)
                {
                    modelemp.d_id = postdata[i].Dep_id;
                    modelemp.position_id = postdata[i].Position_id;
                    modelemp.postid = postdata[i].Post_id;
                    //context.Response.Write(postdata[i].Depname + "@");
                    emp.UpdatePost(modelemp);
                }

                hp.UpdatePostEmp(model);
            }
        }

        //combo
        public string combo(int id)
        {
            DataSet ds = post.GetList(" dep_id=" + id);

            var str = new StringBuilder();

            str.Append("[");
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                str.Append("{id:" + ds.Tables[0].Rows[i]["id"] + ",text:'" + ds.Tables[0].Rows[i]["post_name"] + "'},");
            }
            str.Replace(",", "", str.Length - 1, 1);
            str.Append("]");

            return str.ToString();
        }

        public class PostData
        {
            public int Post_id { get; set; }

            public string Post_name { get; set; }

            public int? Emp_id { get; set; }

            public string Emp_name { get; set; }

            public int? Default_post { get; set; }

            public int? Dep_id { get; set; }

            public string Depname { get; set; }

            public int? Position_id { get; set; }

            public string Position_name { get; set; }
        }
    }
}