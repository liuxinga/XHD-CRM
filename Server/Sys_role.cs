﻿/*
* Sys_role.cs
*
* 功 能： N/A
* 类 名： Sys_role
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-23 18:38:21    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;
using System.Data;
using System.Web;
using System.Web.Script.Serialization;
using XHD.Common;
using XHD.Controller;
using XHD.Model;

namespace XHD.Server
{
    public class Sys_role
    {
        public static BLL.Sys_role role = new BLL.Sys_role();
        public static Model.Sys_role model = new Model.Sys_role();

        public HttpContext Context;
        public int emp_id;
        public string emp_name;
        public Model.hr_employee employee;
        public HttpRequest request;
        public string uid;


        public Sys_role()
        {
        }

        public Sys_role(HttpContext context)
        {
            Context = context;
            request = context.Request;

            var userinfo = new User_info();
            employee = userinfo.GetCurrentEmpInfo(context);

            emp_id = employee.ID;
            emp_name = PageValidate.InputText(employee.name, 50);
            uid = PageValidate.InputText(employee.uid, 50);
        }

        //save
        public void SysSave()
        {
            model.RoleName = PageValidate.InputText(request["T_role"], 250);
            model.RoleSort = int.Parse(request["T_RoleOrder"]);
            model.RoleDscript = PageValidate.InputText(request["T_Descript"], 255);

            string id = PageValidate.InputText(request["id"], 50);

            if (PageValidate.IsNumber(id))
            {
                DataSet ds = role.GetList("RoleID=" + int.Parse(id));
                DataRow dr = ds.Tables[0].Rows[0];
                model.RoleID = int.Parse(id);
                model.UpdateDate = DateTime.Now;
                model.UpdateID = emp_id;
                role.Update(model);
            }
            else
            {
                model.CreateID = emp_id;
                model.CreateDate = DateTime.Now;
                int rid = role.Add(model);

                var auth = new BLL.Sys_data_authority();
                var modelsda = new Model.Sys_data_authority();

                //默认数据权限
                modelsda.Role_id = rid;
                modelsda.Sys_view = 1;
                modelsda.Sys_add = 1;
                modelsda.Sys_edit = 1;
                modelsda.Sys_del = 1;

                modelsda.option_id = 1;
                modelsda.Sys_option = "客户管理";
                auth.Add(modelsda);

                modelsda.option_id = 2;
                modelsda.Sys_option = "跟进管理";
                auth.Add(modelsda);

                modelsda.option_id = 3;
                modelsda.Sys_option = "订单管理";
                auth.Add(modelsda);

                modelsda.option_id = 4;
                modelsda.Sys_option = "合同管理";
                auth.Add(modelsda);
            }
        }

        //validate
        public string Exist()
        {
            DataSet ds1 = role.GetList(" RoleName='" + PageValidate.InputText(request["T_role"], 250) + "'");
            return (ds1.Tables[0].Rows.Count > 0 ? "false" : "true");
        }

        //表格json
        public string grid()
        {
            DataSet ds = role.GetList(0, "", " RoleSort");

            string dt = GetGridJSON.DataTableToJSON(ds.Tables[0]);

            return dt;
        }

        //Form JSON
        public string form(int id)
        {
            DataSet ds = role.GetList(" RoleID=" + id);

            string dt = DataToJson.DataToJSON(ds);

            return dt;
        }

        //del
        public string del(int id)
        {
            bool isdel = role.Delete(id);


            //角色下员工删除
            var rm = new BLL.Sys_role_emp();
            rm.Delete("RoleID=" + id);

            //角色下数据权限删除
            var dataauth = new BLL.Sys_data_authority();
            dataauth.Delete("Role_id=" + id);

            //角色权限
            var auth = new BLL.Sys_authority();
            auth.DeleteWhere("Role_id=" + id);

            if (isdel)
                return ("true");
            return ("false");
        }

        //auth
        public string treegrid(int appid)
        {
            var menu = new BLL.Sys_Menu();

            //string dt1 = 
            DataTable dt = menu.GetList(0,"App_id=" + appid,"Menu_order").Tables[0];
            dt.Columns.Add(new DataColumn("Sysroler", typeof (string)));

            var btn = new BLL.Sys_Button();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataSet ds = btn.GetList(0, "Menu_id=" + dt.Rows[i]["Menu_id"], " Btn_order");
                string roler = "";
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int j = 0; j < ds.Tables[0].Rows.Count; j++)
                    {
                        roler += ds.Tables[0].Rows[j]["Btn_id"] + "|" + ds.Tables[0].Rows[j]["Btn_name"];
                        roler += ",";
                    }
                }
                dt.Rows[i][dt.Columns.Count - 1] = roler;
            }
            string dt1 = "{Rows:[" + GetTasks.GetMenuTree(0, dt) + "]}";
            return dt1;
        }

        //get auth
        public string getauth()
        {
            string postdata = Convert.ToString(HttpContext.Current.Request.QueryString["postdata"]);
            var json = new JavaScriptSerializer();
            var sa = json.Deserialize<save>(postdata);
            var modelauth = new Sys_authority();
            modelauth.Role_id = int.Parse(sa.role_id);
            modelauth.App_ids = sa.app;
            modelauth.Menu_ids = sa.menu;
            modelauth.Button_ids = sa.btn;

            var sysau = new BLL.Sys_authority();

            string roledata = "0|0";
            DataSet ds =
                sysau.GetList("Role_id=" + modelauth.Role_id + " and App_ids='a" +
                              PageValidate.InputText(modelauth.App_ids, int.MaxValue) + ",'");
            if (ds.Tables[0].Rows.Count > 0)
            {
                DataRow dr = ds.Tables[0].Rows[0];
                roledata = dr["Menu_ids"] + "|" + dr["Button_ids"];
            }
            return (roledata);
        }

        // save auth
        public void saveauth()
        {
            string postdata = Convert.ToString(HttpContext.Current.Request.QueryString["postdata"]);
            var json = new JavaScriptSerializer();
            var sa = json.Deserialize<save>(postdata);
            var modelauth = new Sys_authority();
            modelauth.Role_id = int.Parse(sa.role_id);
            modelauth.App_ids = PageValidate.InputText(sa.app, 50);
            modelauth.Menu_ids = PageValidate.InputText(sa.menu, int.MaxValue);
            modelauth.Button_ids = PageValidate.InputText(sa.btn, int.MaxValue);

            var sysau = new BLL.Sys_authority();

            if (!string.IsNullOrEmpty(postdata))
            {
                sysau.DeleteWhere("Role_id=" + modelauth.Role_id + " and App_ids='" +
                                  PageValidate.InputText(modelauth.App_ids, int.MaxValue) + "'");
                sysau.Add(modelauth);


                //日志
                var log = new BLL.Sys_log();
                var modellog = new Model.Sys_log();

                modellog.EventDate = DateTime.Now;
                modellog.UserID = emp_id;
                modellog.IPStreet = request.UserHostAddress;

                modellog.EventType = "权限修改";
                modellog.EventID = modelauth.Role_id.ToString();
                log.Add(modellog);
            }
        }

        public string getEmpMenus()
        {
            if (emp_id == 1)
            {
                return "(4,5,6,34,35,36,37)";
            }
            var auth = new GetAuthorityByUid();

            string txt = auth.GetMenus(emp_id.ToString());

            return txt;
        }

        [Serializable]
        private class save
        {
            public string role_id { get; set; }
            public string app { get; set; }
            public string menu { get; set; }
            public string btn { get; set; }
        }
    }
}