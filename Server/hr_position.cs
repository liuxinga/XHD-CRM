﻿/*
* hr_position.cs
*
* 功 能： N/A
* 类 名： hr_position
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-23 18:38:21    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;
using System.Data;
using System.Text;
using System.Web;
using XHD.Common;
using XHD.Controller;

namespace XHD.Server
{
    public class hr_position
    {
        public static BLL.hr_position position = new BLL.hr_position();
        public static Model.hr_position model = new Model.hr_position();

        public HttpContext Context;
        public int emp_id;
        public string emp_name;
        public Model.hr_employee employee;
        public HttpRequest request;
        public string uid;


        public hr_position()
        {
        }

        public hr_position(HttpContext context)
        {
            Context = context;
            request = context.Request;

            var userinfo = new User_info();
            employee = userinfo.GetCurrentEmpInfo(context);

            emp_id = employee.ID;
            emp_name = PageValidate.InputText(employee.name, 50);
            uid = PageValidate.InputText(employee.uid, 50);
        }

        public string grid()
        {
            string serchtxt = "1=1";
            DataSet ds = position.GetList(0, serchtxt, "[position_order]");
            string dt = GetGridJSON.DataTableToJSON(ds.Tables[0]);
            return dt;
        }

        //save
        public void save()
        {
            model.position_name = PageValidate.InputText(request["T_position"], 255);
            model.position_order = int.Parse(request["T_order"]);
            model.position_level = PageValidate.InputText(request["T_level"], 50);

            string id = PageValidate.InputText(request["id"], 250);

            if (PageValidate.IsNumber(id))
            {
                model.id = int.Parse(id);
                DataSet ds = position.GetList(" id=" + int.Parse(id));
                DataRow dr = ds.Tables[0].Rows[0];
                position.Update(model);

                //日志
                var log = new sys_log();

                int UserID = emp_id;
                string UserName = emp_name;
                string IPStreet = request.UserHostAddress;
                string EventTitle = model.position_name;
                string EventType = "职位修改";
                int EventID = model.id;
                string Log_Content = null;

                if (dr["position_name"].ToString() != request["T_position"])
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "职务名称", dr["position_name"], request["T_position"]);

                if (dr["position_level"].ToString() != request["T_level"])
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "职务级别", dr["position_level"], request["T_level"]);

                if (dr["position_order"].ToString() != request["T_order"])
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "行号", dr["position_order"], request["T_order"]);

                if (!string.IsNullOrEmpty(Log_Content))
                    log.Add_log(UserID, UserName, IPStreet, EventTitle, EventType, EventID, Log_Content);
            }
            else
            {
                model.isDelete = 0;
                model.create_id = emp_id;
                model.create_date = DateTime.Now;
                position.Add(model);
            }
        }

        //Form JSON
        public string form(int id)
        {
            DataSet ds = position.GetList("id=" + id);

            string dt = DataToJson.DataToJSON(ds);

            return dt;
        }

        public string del(int id)
        {
            var emp = new BLL.hr_employee();
            string EventType = "职务删除";
            DataSet ds = position.GetList(" id=" + id);
            if (emp.GetList("position_id = " + id).Tables[0].Rows.Count > 0)
            {
                //含有员工信息不能删除
                return ("false:emp");
            }
            bool isdel = position.Delete(id);
            if (isdel)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    int UserID = emp_id;
                    string UserName = emp_name;
                    string IPStreet = request.UserHostAddress;
                    int EventID = id;
                    string EventTitle = ds.Tables[0].Rows[i]["position_name"].ToString();

                    var log = new sys_log();
                    log.Add_log(UserID, UserName, IPStreet, EventTitle, EventType, EventID, null);
                }
                return ("true");
            }
            return ("false");
        }


        public string combo()
        {
            DataSet ds = position.GetList(0, "", "position_level");
            var str = new StringBuilder();
            str.Append("[");
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                str.Append("{id:" + ds.Tables[0].Rows[i]["id"] + ",text:'" + ds.Tables[0].Rows[i]["position_name"] +
                           "'},");
            }
            str.Replace(",", "", str.Length - 1, 1);
            str.Append("]");
            return str.ToString();
        }

        public string getlevel(int id)
        {
            var hz = new BLL.hr_position();
            DataSet ds = hz.GetList("id=" + id);

            if (ds.Tables[0].Rows.Count > 0)
            {
                return ds.Tables[0].Rows[0]["position_level"].ToString();
            }
            return ("-1");
        }
    }
}