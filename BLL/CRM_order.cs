﻿/*
* CRM_order.cs
*
* 功 能： N/A
* 类 名： CRM_order
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-23 19:49:34    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System.Data;
using XHD.Common;

namespace XHD.BLL
{
    /// <summary>
    ///     CRM_order
    /// </summary>
    public class CRM_order
    {
        private readonly DAL.CRM_order dal = new DAL.CRM_order();

        #region  BasicMethod		

        /// <summary>
        ///     增加一条数据
        /// </summary>
        public int Add(Model.CRM_order model)
        {
            return dal.Add(model);
        }

        /// <summary>
        ///     更新一条数据
        /// </summary>
        public bool Update(Model.CRM_order model)
        {
            return dal.Update(model);
        }

        /// <summary>
        ///     删除一条数据
        /// </summary>
        public bool Delete(int id)
        {
            return dal.Delete(id);
        }

        /// <summary>
        ///     删除一条数据
        /// </summary>
        public bool DeleteList(string idlist)
        {
            return dal.DeleteList(PageValidate.SafeLongFilter(idlist, 0));
        }


        /// <summary>
        ///     获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }

        /// <summary>
        ///     获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            return dal.GetList(Top, strWhere, filedOrder);
        }

        /// <summary>
        ///     获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        /// <summary>
        ///     分页获取数据列表
        ///     分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder, out string Total)
        {
            return dal.GetList(PageSize, PageIndex, strWhere, filedOrder, out Total);
        }

        #endregion  BasicMethod

        #region  ExtensionMethod

        /// <summary>
        ///     批量
        /// </summary>
        public bool Update_batch(Model.CRM_order model)
        {
            return dal.Update_batch(model);
        }

        /// <summary>
        ///     更新发票金额
        /// </summary>
        /// <param name="orderid"></param>
        /// <returns></returns>
        public bool UpdateInvoice(int orderid)
        {
            return dal.UpdateInvoice(orderid);
        }

        /// <summary>
        ///     更新收款金额
        /// </summary>
        /// <param name="orderid"></param>
        /// <returns></returns>
        public bool UpdateReceive(int orderid)
        {
            return dal.UpdateReceive(orderid);
        }

        /// <summary>
        ///  更新收款和发票客户ID
        /// </summary>
        /// <param name="orderid"></param>
        /// <returns></returns>
        public bool UpdateCustomerID(int orderid, int CustomerID)
        {
            return dal.UpdateCustomerID(orderid, CustomerID);
        }
        #endregion  ExtensionMethod
    }
}