﻿/*
* CRM_SMS_details.cs
*
* 功 能： N/A
* 类 名： CRM_SMS_details
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015/8/19 21:46:12    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com  www.xhdoa.com. All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：小黄豆                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
using System.Data;
using System.Collections.Generic;
using XHD.Common;
using XHD.Model;
namespace XHD.BLL
{
	/// <summary>
	/// CRM_SMS_details
	/// </summary>
	public partial class CRM_SMS_details
	{
		private readonly XHD.DAL.CRM_SMS_details dal=new XHD.DAL.CRM_SMS_details();
		public CRM_SMS_details()
		{}
		#region  BasicMethod

		/// <summary>
		/// 增加一条数据
		/// </summary>
		public bool Add(XHD.Model.CRM_SMS_details model)
		{
			return dal.Add(model);
		}

		
		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete()
		{
			//该表无主键信息，请自定义主键/条件字段
			return dal.Delete();
		}

		

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}
		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}
		

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		

		#endregion  BasicMethod
		#region  ExtensionMethod

		#endregion  ExtensionMethod
	}
}

