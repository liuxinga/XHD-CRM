<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <meta http-equiv="X-UA-Compatible" content="ie=8 chrome=1" />
    <link href="../../lib/ligerUI/skins/ext/css/ligerui-all.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/input.css" rel="stylesheet" />

    <script src="../../lib/jquery/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script src="../../lib/ligerUI/js/plugins/ligerForm.js" type="text/javascript"></script>
    <script src="../../lib/ligerUI/js/plugins/ligerComboBox.js" type="text/javascript"></script>
    <script src="../../lib/ligerUI/js/plugins/ligerRadio.js" type="text/javascript"></script>
    <script src="../../lib/ligerUI/js/plugins/ligerSpinner.js" type="text/javascript"></script>
    <script src="../../lib/ligerUI/js/plugins/ligerTextBox.js" type="text/javascript"></script>
    <script src="../../lib/ligerUI/js/plugins/ligerDateEditor.js" type="text/javascript"></script>
    <script src="../../lib/ligerUI/js/plugins/ligerCheckBox.js" type="text/javascript"></script>

    <script src="../../lib/ligerUI/js/plugins/ligerTree.js" type="text/javascript"></script>
    <script src="../../lib/ligerUI/js/plugins/ligerGrid.js" type="text/javascript"></script>
    <script src="../../lib/ligerUI/js/plugins/ligerDialog.js" type="text/javascript"></script>
    <script src="../../lib/ligerUI/js/plugins/ligerDrag.js" type="text/javascript"></script>

    <script src="../../lib/jquery-validation/jquery.validate.js" type="text/javascript"></script>
    <script src="../../lib/jquery-validation/jquery.metadata.js" type="text/javascript"></script>
    <script src="../../lib/jquery-validation/messages_cn.js" type="text/javascript"></script>
    <script src="../../lib/ligerUI/js/common.js" type="text/javascript"></script>
    <script src="../../lib/ligerUI/js/plugins/ligerTip.js" type="text/javascript"></script>
    <script src="../../lib/jquery.form.js" type="text/javascript"></script>
    <script src="../../JS/Toolbar.js" type="text/javascript"></script>
    <script src="../../JS/XHD.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $.metadata.setType("attr", "validate");
            XHD.validate($(form1));

            $("form").ligerForm();

            loadForm(getparastr("id"));
        })
        function f_save() {
            if ($(form1).valid()) {
                var sendtxt = "&id=" + getparastr("id");
                return $("form :input").fieldSerialize() + sendtxt;
            }
        }

        function loadForm(oaid) {
            $.ajax({
                type: "GET",
                url: "Sys_mail_type.form.xhd", /* 注意后面的名字对应CS的方法名称 */
                data: { id: oaid, rnd: Math.random() }, /* 注意参数的格式和名称 */
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    var obj = eval(result);
                    for (var n in obj) {
                        if (obj[n] == "null" || obj[n] == null)
                            obj[n] = "";
                    }
                    //alert(obj.constructor); //String 构造函数
                    $("#T_name").val(obj.mail_type);
                    $("#T_smtp").val(obj.smtp);
                    $("#T_port").val(obj.smtp_port);
                    $("#T_order").val(obj.mail_order);

                    if(obj.use_ssl)
                    $("#T_ssl").ligerGetComboBoxManager().selectValue(obj.use_ssl);
                }
            });

        }
       
        function remotesite() {
            var url = "CRM_Customer.validate.xhd?T_cid=" + getparastr("cid") + "&rnd=" + Math.random();
            return url;
        }

    </script>
</head>
<body>
    <form id="form1" onsubmit="return false">

        <table style="width: 600px; margin: 5px;" class="bodytable0">
            <tr>
                <td class="table_title1" colspan="2">邮件类别</td>
            </tr>
            <tr>
                <td class="table_label"> 邮箱类别：</td>
                <td><input id="T_name" name="T_name" type="text" ltype="text" ligerui="{width:180}"  validate="{required:true}" />
                </td>
            </tr>
             <tr>
                <td class="table_label"> smtp：</td>
                <td><input id="T_smtp" name="T_smtp" type="text" ltype="text" ligerui="{width:180}"  validate="{required:true}" />
                </td>
            </tr>
             <tr>
                <td class="table_label"> 端口：</td>
                <td><input id="T_port" name="T_port" type="text" ltype="spinner" ligerui="{width:180,type:'int',maxValue:65535,minValue:0 }"  validate="{required:true}" />
                </td>
            </tr>
             <tr>
                <td class="table_label"> ssl：</td>
                <td><input id="T_ssl" name="T_ssl" type="text" ltype="select" ligerui="{width:180,data:[{id:0,text:'不启用'},{id:1,text:'启用'}],initValue:0 }" />
                </td>
            </tr>

             <tr>
                <td class="table_label"> 排序：</td>
                <td>
                    <input id="T_order" name="T_order" type="text" ltype="spinner" ligerui="{width:180,type:'int',minValue:0 }"  value="20" validate="{required:true}" />
                </td>
            </tr>

        </table>

    </form>
</body>
</html>
