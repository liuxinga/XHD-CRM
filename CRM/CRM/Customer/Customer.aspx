<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <meta http-equiv="X-UA-Compatible" content="IE=8" />
    <link href="../../lib/ligerUI/skins/ext/css/ligerui-all.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/input.css" rel="stylesheet" />

    <script src="../../lib/jquery/jquery-1.3.2.min.js" type="text/javascript"> </script>
    <script src="../../lib/ligerUI/js/plugins/ligerLayout.js" type="text/javascript"> </script>
    <script src="../../lib/ligerUI/js/plugins/ligerGrid.js" type="text/javascript"> </script>
    <script src="../../lib/ligerUI/js/plugins/ligerForm.js" type="text/javascript"> </script>
    <script src="../../lib/ligerUI/js/plugins/ligerCheckBox.js" type="text/javascript"> </script>
    <script src="../../lib/ligerUI/js/plugins/ligerComboBox.js" type="text/javascript"> </script>
    <script src="../../lib/ligerUI/js/plugins/ligerDateEditor.js" type="text/javascript"> </script>
    <script src="../../lib/ligerUI/js/plugins/ligerRadio.js" type="text/javascript"> </script>
    <script src="../../lib/ligerUI/js/plugins/ligerTextBox.js" type="text/javascript"> </script>
    <script src="../../lib/ligerUI/js/plugins/ligerSpinner.js" type="text/javascript"> </script>
    <script src="../../lib/ligerUI/js/plugins/ligerTree.js" type="text/javascript"> </script>
    <script src="../../lib/ligerUI/js/plugins/ligerDialog.js" type="text/javascript"> </script>
    <script src="../../lib/ligerUI/js/plugins/ligerDrag.js" type="text/javascript"> </script>
    <script src="../../lib/ligerUI/js/plugins/ligerResizable.js" type="text/javascript"> </script>
    <script src="../../lib/ligerUI/js/plugins/ligerTip.js" type="text/javascript"> </script>
    <script src="../../lib/jquery.form.js" type="text/javascript"> </script>
    <script src="../../lib/ligerUI/js/plugins/ligerToolBar.js" type="text/javascript"> </script>
    <script src="../../JS/XHD.js" type="text/javascript"> </script>
    <script src="../../lib/ligerUI/js/plugins/ligerMenu.js" type="text/javascript"> </script>
    <script type="text/javascript">
        if (top.location == self.location) top.location = "../../default.aspx";
        var manager;
        var manager1;
        var loaddiff = 150;
        $(function () {

            initLayout();
            $(window).resize(function () {
                initLayout();
            });

            grid();
            $("#grid").height(document.documentElement.clientHeight - $(".toolbar").height());
            $('form').ligerForm();
            toolbar();


            //var tt = test(jsonObj.Rows, 3);
            //alert(JSON.stringify(tt));
        });
       
        function grid() {
            $("#maingrid4").ligerGrid({
                columns: [
                    { display: '序号', name: 'id', width: 50, render: function (item, i) { return item.n;} },
                    {
                        display: '客户', name: 'Customer', width: 200, align: 'left', render: function (item) {
                            var html = "<a href='javascript:void(0)' onclick=view(1," + item.id + ")>";
                            if (item.Customer)
                                html += item.Customer;
                            html += "</a>";
                            return html;
                        }
                    },
                    { display: '电话', name: 'tel', width: 120, align: 'right' },
                    { display: '客户类型', name: 'CustomerType_id', width: 80, render: function (item, i) { return item.CustomerType } },
                    { display: '客户类别', name: 'CustomerLevel_id', width: 80, render: function (item, i) { return item.CustomerLevel } },
                    { display: '客户来源', name: 'CustomerSource_id', width: 80, render: function (item, i) { return item.CustomerSource } },
                    { display: '省份', name: 'Provinces_id', width: 80, render: function (item, i) { return item.Provinces } },
                    { display: '城市', name: 'City_id', width: 80, render: function (item, i) { return item.City } },
                    { display: '所属行业', name: 'industry_id', width: 80, render: function (item, i) { return item.industry } },
                    { display: '部门', name: 'Department_id', width: 80, render: function (item, i) { return item.Department } },
                    { display: '员工', name: 'Employee_id', width: 80, render: function (item, i) { return item.Employee } },
                    { display: '客源状态', name: 'privatecustomer', width: 60 },
                    {
                        display: '最后跟进', name: 'lastfollow', width: 90, render: function (item) {
                            var lastfollow = formatTimebytype(item.lastfollow, 'yyyy-MM-dd');
                            if (lastfollow == "1900-01-01")
                                lastfollow = "";
                            return lastfollow;
                        }
                    },
                    {
                        display: '创建时间',
                        name: 'Create_date',
                        width: 90,
                        render: function (item) {
                            var Create_date = formatTimebytype(item.Create_date, 'yyyy-MM-dd');
                            return Create_date;
                        }
                    }
                ],
                onbeforeLoaded: function (grid, data) {
                    startTime = new Date();
                },
                //fixedCellHeight:false,
                onSelectRow: function (data, rowindex, rowobj) {
                    var manager = $("#maingrid5").ligerGetGridManager();
                    manager.showData({ Rows: [], Total: 0 });
                    var url = "CRM_Follow.grid.xhd?customer_id=" + data.id;
                    manager.GetDataByURL(url);
                },
               
                rowtype: "CustomerType",
                dataAction: 'server',
                pageSize: 30,
                pageSizeOptions: [20, 30, 50, 100],
                url: "CRM_Customer.grid.xhd?rnd=" + Math.random(),
                width: '100%',
                height: '65%',
                heightDiff: -1,
                onRClickToSelect: true,
                onContextmenu: function (parm, e) {
                    actionCustomerID = parm.data.id;
                    menu.show({ top: e.pageY, left: e.pageX });
                    return false;
                },
                onAfterShowData: function (grid) {
                    $("tr[rowtype='已成交']").addClass("l-treeleve1").removeClass("l-grid-row-alt");
                    var nowTime = new Date();
                    loaddiff = nowTime - startTime;
                    //alert('加载数据耗时：' + (nowTime - startTime));
                },
               
                detail: {
                    onShowDetail: function (r, p) {
                        for (var n in r) {
                            if (r[n] == null) r[n] = "";
                        }
                        var grid = document.createElement('div');
                        $(p).append(grid);
                        $(p).css({ "overflow": "scroll" });
                        $(grid).css('margin', 3).ligerGrid({
                            columns: [
                                { display: '行号', width: 50, render: function (item, i) { return i + 1; } },
                                { display: '联系人', name: 'C_name', width: 100 },
                                { display: '部门', name: 'C_department', width: 100 },
                                { display: '职务', name: 'C_position', width: 100 },
                                { display: '性别', name: 'C_sex', width: 50 },
                                //{ display: '所属公司', name: 'C_companyname', width: 180 },
                                { display: '手机', name: 'C_mob', width: 120 },
                                { display: '电话', name: 'C_tel', width: 100 },
                                { display: 'QQ', name: 'C_QQ', width: 100 },
                                { display: 'Email', name: 'C_email', width: 180 }
                            ],
                            allowHideColumn:false,
                            usePager: false,                            
                            url: "CRM_Contact.grid.xhd?customerid=" + r.id,
                            width: '99%',
                            height: '100px',
                            heightDiff: 0
                        });
                    }
                }
            });
            $("#maingrid5").ligerGrid({
                columns: [
                    { display: '序号', width: 40, render: function (item, i) { return i + 1; } },
                    {
                        display: '跟进内容',
                        name: 'Follow',
                        align: 'left',
                        width: 400,
                        render: function (item) {
                            var html = "<div class='abc'><a href='javascript:void(0)' onclick=view(2," + item.id + ")>";
                            if (item.Follow)
                                html += item.Follow;
                            html += "</a></div>";
                            return html;
                        }
                    },
                     {
                         display: '联系人', name: 'Contact_id', width: 100, render: function (item) {
                             var html = "<a href='javascript:void(0)' onclick=view(3," + item.Contact_id + ")>";
                             if (item.Contact)
                                 html += item.Contact;
                             html += "</a>";
                             return html;
                         }
                     },
                     { display: '跟进目的', name: 'Follow_aim_id', width: 60, render: function (item, i) { return item.Follow_aim } },
                    {
                        display: '跟进时间',
                        name: 'Follow_date',
                        width: 140,
                        render: function (item) {
                            return formatTimebytype(item.Follow_date, 'yyyy-MM-dd hh:mm');
                        }
                    },
                    { display: '跟进方式', name: 'Follow_Type_id', width: 60, render: function (item, i) { return item.Follow_Type } },
                    { display: '跟进部门', name: 'Department_id', width: 80, render: function (item, i) { return item.Department } },
                    { display: '跟进人', name: 'Employee_id', width: 80, render: function (item, i) { return item.Employee } }
                ],
                onbeforeLoaded: function (grid, data) {
                    startTime = new Date();
                },
                onAfterShowData: function (grid) {
                    $(".abc").hover(function (e) {
                        $(this).ligerTip({ content: $(this).text(), width: 200, distanceX: event.clientX - $(this).offset().left - $(this).width() + 15 });
                    }, function (e) {
                        $(this).ligerHideTip(e);
                    });
                },
                dataAction: 'server',
                pageSize: 30,
                pageSizeOptions: [20, 30, 50, 100],
                //checkbox:true,
                url: "CRM_Follow.grid.xhd?customer_id=0",
                width: '100%',
                height: '100%',
                //title: "跟进信息",
                heightDiff: -1,
                onRClickToSelect: true,
                

                onContextmenu: function (parm, e) {
                    actionCustomerID = parm.data.id;
                    menu1.show({ top: e.pageY, left: e.pageX });
                    return false;
                }
            });
        }

        function toolbar() {
            $.getJSON("toolbar.GetSys.xhd?mid=4&rnd=" + Math.random(), function (data, textStatus) {
                //alert(data);
                var items = [];
                var arr = data.Items;
                for (var i = 0; i < arr.length; i++) {
                    arr[i].icon = "../../" + arr[i].icon;
                    items.push(arr[i]);
                }
                items.push({
                    type: 'serchbtn',
                    text: '高级搜索',
                    icon: '../../images/search.gif',
                    disable: true,
                    click: function () {
                        serchpanel();
                    }
                });
                items.push({ type: "filter", icon: '../../images/icon/51.png', title: "帮助", click: function () { help(); } })
                $("#toolbar").ligerToolBar({
                    items: items
                });
                menu = $.ligerMenu({
                    width: 120,
                    items: getMenuItems(data)
                });

            });
            $.getJSON("toolbar.GetSys.xhd?mid=6&rnd=" + Math.random(), function (data, textStatus) {
                //alert(data);
                var items = [];
                var arr = data.Items;
                for (var i = 0; i < arr.length; i++) {
                    arr[i].icon = "../../" + arr[i].icon;
                    items.push(arr[i]);
                }
                $("#toolbar1").ligerToolBar({
                    items: items
                });
                menu1 = $.ligerMenu({
                    width: 120,
                    items: getMenuItems(data)
                });

                $("#maingrid4").ligerGetGridManager().onResize();
                $("#maingrid5").ligerGetGridManager().onResize();
            });
        }

        function initSerchForm() {
            var a = $('#T_City').ligerComboBox({ width: 96, emptyText: '（空）' });
            var b = $('#T_Provinces').ligerComboBox({
                width: 97,

                url: "Param_City.combo1.xhd?rnd=" + Math.random(),
                onSelected: function (newvalue) {
                    $.get("Param_City.combo2.xhd?pid=" + newvalue + "&rnd=" + Math.random(), function (data, textStatus) {
                        a.setData(eval(data));
                    });
                },
                emptyText: '（空）'
            });
            $('#customertype').ligerComboBox({ width: 97, emptyText: '（空）', url: "Param_SysParam.combo.xhd?parentid=1&rnd=" + Math.random() });
            $('#customerlevel').ligerComboBox({ width: 96, emptyText: '（空）', url: "Param_SysParam.combo.xhd?parentid=2&rnd=" + Math.random() });
            $('#cus_sourse').ligerComboBox({ width: 196, emptyText: '（空）', url: "Param_SysParam.combo.xhd?parentid=3&rnd=" + Math.random() });
            $('#industry').ligerComboBox({ width: 120, emptyText: '（空）', url: "Param_SysParam.combo.xhd?parentid=8&rnd=" + Math.random() });
            var e = $('#employee').ligerComboBox({ width: 96, emptyText: '（空）' });
            var f = $('#department').ligerComboBox({
                width: 97,
                selectBoxWidth: 240,
                selectBoxHeight: 200,
                valueField: 'id',
                textField: 'text',
                treeLeafOnly: false,
                tree: {
                    url: 'hr_department.tree.xhd?rnd=' + Math.random(),
                    idFieldName: 'id',
                    //parentIDFieldName: 'pid',
                    checkbox: false
                },
                onSelected: function (newvalue) {
                    $.get("hr_employee.combo.xhd?did=" + newvalue + "&rnd=" + Math.random(), function (data, textStatus) {
                        e.setData(eval(data));
                    });
                }
            });
        }

        function serchpanel() {
            initSerchForm();
            if ($(".az").css("display") == "none") {
                $("#grid").css("margin-top", $(".az").height() + "px");
                $("#maingrid4").ligerGetGridManager().onResize();
                $("#maingrid5").ligerGetGridManager().onResize();
            } else {
                $("#grid").css("margin-top", "0px");
                $("#maingrid4").ligerGetGridManager().onResize();
                $("#maingrid5").ligerGetGridManager().onResize();
            }
            $("#company").focus();
        }

        $(document).keydown(function (e) {
            if (e.keyCode == 13 && e.target.applyligerui) {
                doserch();
            }
        });

        function doserch() {
            var sendtxt = "&rnd=" + Math.random();
            var serchtxt = $("#serchform :input").fieldSerialize() + sendtxt;
            //alert(serchtxt);           
            var manager = $("#maingrid4").ligerGetGridManager();
            manager.GetDataByURL("CRM_Customer.grid.xhd?" + serchtxt);

        }

        function doclear() {
            $("input:hidden", "#serchform").val("");
            $("input:text", "#serchform").val("");
            $(".l-selected").removeClass("l-selected");
        }



        function toimport() {
            f_openWindow('crm/customer/customer_import.aspx', '客户导入', 540, 295);
        }

        function add() {
            f_openWindow("CRM/Customer/Customer_add.aspx", "新增客户", 770, 495, f_save);
        }

        function edit() {
            var manager = $("#maingrid4").ligerGetGridManager();
            var row = manager.getSelectedRow();
            if (row) {
                f_openWindow('CRM/Customer/Customer_add.aspx?cid=' + row.id, "修改客户", 770, 490, f_save);
            } else {
                $.ligerDialog.warn('请选择行！');
            }
        }

        function del() {
            var manager = $("#maingrid4").ligerGetGridManager();
            var row = manager.getSelectedRow();
            if (row) {
                $.ligerDialog.confirm("确定删除？", function (yes) {
                    if (yes) {
                        $.ajax({
                            url: "CRM_Customer.AdvanceDelete.xhd",
                            type: "POST",
                            data: { id: row.id, rnd: Math.random() },
                            success: function (responseText) {
                                if (responseText == "true") {
                                    f_reload();
                                    f_followreload();
                                } else if (responseText == "delfalse") {
                                    top.$.ligerDialog.error('权限不够，删除失败！');
                                } else if (responseText == "false") {
                                    top.$.ligerDialog.error('未知错误，删除失败！');
                                } else {
                                    top.$.ligerDialog.warn('此客户下含有 ' + responseText + '，删除失败！请先先将这些数据删除。');
                                }

                            },
                            error: function () {
                                top.$.ligerDialog.error('删除失败！');
                            }
                        });
                    }
                });
            } else {
                $.ligerDialog.warn("请选择客户");
            }
        }

        function toexport() {
            var sendtxt = "&rnd=" + Math.random();
            var serchtxt = $("#serchform :input").fieldSerialize() + sendtxt;
            var url = "CRM_Customer.ToExcel.xhd?" + serchtxt;

            window.open(url);
        }

        function f_save(item, dialog) {
            var issave = dialog.frame.f_save();
            if (issave) {
                dialog.close();

                $.ajax({
                    url: "CRM_Customer.save.xhd",
                    type: "POST",
                    data: issave,
                    beforesend: function () {
                        top.$.ligerDialog.waitting('数据保存中,请稍候...');
                    },
                    success: function (responseText) {
                        f_reload();
                    },
                    error: function () {
                        top.$.ligerDialog.error('操作失败！');
                    },
                    complete: function () {
                        top.$.ligerDialog.closeWaitting();
                    }
                });

            }
        }

        function f_reload() {
            var manager = $("#maingrid4").ligerGetGridManager();
            manager.loadData(true);
        }
        
        function addfollow() {
            var manager = $("#maingrid4").ligerGetGridManager();
            var row = manager.getSelectedRow();
            if (row) {
                f_openWindow("CRM/contact/Customer_follow_add.aspx?cid=" + row.id, "新增跟进", 630, 400,f_savefollow);
            } else {
                $.ligerDialog.warn('请选择客户！');
            }
        }

        function editfollow() {
            var manager = $("#maingrid5").ligerGetGridManager();
            var row = manager.getSelectedRow();
            if (row) {
                f_openWindow('CRM/contact/Customer_follow_add.aspx?fid=' + row.id + "&cid=" + row.Customer_id, "修改跟进", 630, 400,f_savefollow);
            } else {
                $.ligerDialog.warn('请选择跟进！');
            }
        }

        function delfollow() {
            var manager = $("#maingrid5").ligerGetGridManager();
            var row = manager.getSelectedRow();
            if (row) {
                $.ligerDialog.confirm("跟进删除无法恢复，确定删除？", function (yes) {
                    if (yes) {
                        $.ajax({
                            url: "CRM_Follow.del.xhd",
                            type: "POST",
                            data: { id: row.id, rnd: Math.random() },
                            success: function (responseText) {
                                if (responseText == "true") {
                                    f_followreload();
                                    f_reload();
                                } else {
                                    top.$.ligerDialog.error('删除失败！');
                                }

                            },
                            error: function () {
                                top.$.ligerDialog.error('删除失败！');
                            }
                        });
                    }
                });
            } else {
                $.ligerDialog.warn("请选择跟进");
            }
        }

        function f_savefollow(item, dialog) {
            var issave = dialog.frame.f_save();
            if (issave) {
                dialog.close();
                $.ligerDialog.waitting('数据保存中,请稍候...');
                $.ajax({
                    url: "CRM_Follow.save.xhd",
                    type: "POST",
                    data: issave,
                    success: function (responseText) {
                        $.ligerDialog.closeWaitting();
                        f_followreload();
                    },
                    error: function () {
                        $.ligerDialog.closeWaitting();
                        $.ligerDialog.error('操作失败！');
                    }
                });

            }
        }

        function f_followreload() {
            var manager5 = $("#maingrid5").ligerGetGridManager();
            manager5.loadData(true);

            var manager4 = $("#maingrid4").ligerGetGridManager();
            var row = manager4.getSelectedRowObj();
            var rowid = $(row).attr("rowid");
            manager4.loadData(true);

            setTimeout(function () { $("tr[rowid=" + rowid + "]", "#maingrid4").addClass("l-selected") }, loaddiff*1.2);
            //alert($(row).attr("class"));
        }

        function help(parameters) {
            $.ligerDialog.question("此界面是小黄豆CRM的客户管理界面，上面部分是客户列表，下面部分是跟进。点击客户，可以加载跟进信息。具体请访问官方教程：<br/><br/><a href='http://www.xhdcrm.com/khgl/8.html' target='_blank'>客户管理：客户列表教程</a>", "提示");
          
        }
    </script>
    <style type="text/css">
        .l-treeleve1 { background: yellow; }

        .l-treeleve2 { background: yellow; }

        .l-treeleve3 { background: #eee; }
    </style>
</head>
<body>
    <form id="form1" onsubmit=" return false ">
        <div id="toolbar"></div>

        <div id="grid">
            <div id="maingrid4" style="margin: -1px; min-width: 800px;"></div>
            <div id="toolbar1"></div>
            <div id="Div1" style="position: relative;">
                <div id="maingrid5" style="margin: -1px -1px;"></div>
            </div>
        </div>


    </form>
    <div class="az">
        <form id='serchform'>
            <table style='width: 960px' class="bodytable1">
                <tr>
                    <td>
                        <div style='float: right; text-align: right; width: 60px;'>客户名称：</div>
                    </td>
                    <td>
                        <input type='text' id='company' name='company' ltype='text' ligerui='{width:120}' /></td>

                    <td>
                        <div style='float: right; text-align: right; width: 60px;'>客户类型：</div>
                    </td>
                    <td>
                        <div style='float: left; width: 100px;'>
                            <input type='text' id='customertype' name='customertype' />
                        </div>
                        <div style='float: left; width: 98px;'>
                            <input type='text' id='customerlevel' name='customerlevel' />
                        </div>
                    </td>
                    <td>
                        <div style='float: right; text-align: right; width: 60px;'>录入时间：</div>
                    </td>
                    <td>
                        <div style='float: left; width: 100px;'>
                            <input type='text' id='startdate' name='startdate' ltype='date' ligerui='{width:97}' />
                        </div>
                        <div style='float: left; width: 98px;'>
                            <input type='text' id='enddate' name='enddate' ltype='date' ligerui='{width:96}' />
                        </div>
                    </td>
                    <td>
                        <input id='keyword' name="keyword" type='text' ltype='text' ligerui='{width:196, nullText: "输入关键词搜索地址、描述、备注"}' />
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style='float: right; text-align: right; width: 60px;'>所属行业：</div>
                    </td>
                    <td>
                        <input id='industry' name="industry" type='text' /></td>

                    <td>
                        <div style='float: right; text-align: right; width: 60px;'>所属地区：</div>
                    </td>
                    <td>
                        <div style='float: left; width: 100px;'>
                            <input type='text' id='T_Provinces' name='T_Provinces' />
                        </div>
                        <div style='float: left; width: 98px;'>
                            <input type='text' id='T_City' name='T_City' />
                        </div>
                    </td>
                    <td>
                        <div style='float: right; text-align: right; width: 60px;'>最后跟进：</div>
                    </td>
                    <td>
                        <div style='float: left; width: 100px;'>
                            <input type='text' id='startfollow' name='startfollow' ltype='date' ligerui='{width:97}' />
                        </div>
                        <div style='float: left; width: 98px;'>
                            <input type='text' id='endfollow' name='endfollow' ltype='date' ligerui='{width:96}' />
                        </div>
                    </td>

                </tr>
                <tr>
                    <td>
                        <div style='float: right; text-align: right; width: 60px;'>电话：</div>
                    </td>
                    <td>
                        <input type='text' id='tel' name='tel' ltype='text' ligerui='{width:120}' />
                    </td>

                    <td>
                        <div style='float: right; text-align: right; width: 60px;'>客户来源：</div>
                    </td>
                    <td>
                        <input type='text' id='cus_sourse' name='cus_sourse' />
                    </td>
                    <td>
                        <div style='float: right; text-align: right; width: 60px;'>业务员：</div>
                    </td>
                    <td>
                        <div style='float: left; width: 100px;'>
                            <input type='text' id='department' name='department' />
                        </div>
                        <div style='float: left; width: 98px;'>
                            <input type='text' id='employee' name='employee' />
                        </div>
                    </td>
                    <td>

                        <input id='Button2' type='button' value='重置' style='height: 24px; width: 80px;'
                            onclick=" doclear() " />
                        <input id='Button1' type='button' value='搜索' style='height: 24px; width: 80px;' onclick=" doserch() " />
                    </td>
                </tr>
            </table>
        </form>
    </div>

    <form id='toexcel'></form>
</body>
</html>
